const http = require('http')

var app = http.createServer(function (req, res) {
    // als req.url == api/person, dan: toon info van een persoon
    if (req.url === '/api/person') {
        let person = {
            name: 'moi',
            hometown: 'Breda'
        }
        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.end(JSON.stringify(person))
    } else if (req.url === '/api/movie') {
        let movie = {
            title: 'Avengers Endgame',
            homeyear: 2019
        }
        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.end(JSON.stringify(movie))
    } else {
        console.log('requested url: ' + req.url)
        res.writeHead(404, { 'Content-Type': 'text/plain' })
        // var jsonMessage = {
        //     error: 'url not found'
        // }
        // res.write(JSON.stringify(jsonMessage))
        res.end('url not found')
    }
})

module.exports = app
