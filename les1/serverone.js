var http = require('http')

function onRequest(req, res) {
    console.log("Er was een request voor: " + req.url)
    // console.log(req)
    res.writeHead(200, { 'Content-Type': 'text/plain' })
    res.write('Hello world!')
    res.end()
    // console.log(res)
}

http.createServer(onRequest).listen(3000)

console.log('De server is listening (on port 3000)')
