var sayHello = function sayHello() {
    console.log('Hello ')
}

function greeting(name) {
    console.log(name)
}

// module.exports.sayHello = sayHello
// module.exports.greeting = greeting

module.exports = {
    sayHello: sayHello,
    greeting: greeting
}

// sayHello()
// var name = 'Jan'
// setTimeout(sayHello, 2000)
// greeting(name)


