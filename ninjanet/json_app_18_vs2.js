const http = require('http')

function onRequest(req, res) {
    console.log('request url: ' + req.url)
    res.writeHead(200, { 'Content-Type': 'text/plain' })
var movie = {
    title: 'Avengers Endgame',
    year: 2019,
    duration: 182,
    rating: 8.2,
    categories: ['fantasy', 'science fiction']
}

    res.write('movies info');
    res.end(JSON.stringify(movie));
}


http.createServer(onRequest).listen(3050, () => {
    console.log('Running on port 3050')
})
