events = require('events');
util = require('util');

var Person = function (name) {
    this.name = name;
};

util.inherits(Person, events.EventEmitter);

var james = new Person('james');
var ann = new Person('ann');
var robert = new Person('robert');

var people = [james, ann, robert];

people.forEach(function (person) {
    person.on('speak', function (message) {
        console.log(person.name + 'says: ' + message);
    })

});

james.emit('speak', 'Hi to you all');
