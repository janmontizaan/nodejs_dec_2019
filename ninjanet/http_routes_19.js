const http = require('http')
const port = 3000

var app = http.createServer(function (req, res) {
    console.log('request was made: ' + req.url)

    if (req.url === '/home' || req.url === '/') {
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.end('response to: ' + req.url)
    } else if (req.url === '/api/movies') {
        res.writeHead(200, { 'Content-Type': 'application/json' })
        let movies = [{ title: 'Spider man', year: 2018 }, { title: 'Avengers', year: 2019 }]
        res.end(JSON.stringify(movies))
    } else {
        res.writeHead(404, { 'Content-Type': 'text/plain' })
        res.end('url not found')
    }
});

var server = app.listen(port, function () {
    console.log('app running on port: ' + port)
})
    .on('error', (e) => {
        console.error(`Got error: ${e.message}`);
    });


    // app used in server_http_routes_19:
// module.exports = app

// // exports possibilities:
// module.exports.app = app
// module.exports.port = port

// module.exports = {
//     app: app,
//     port: port
// }

